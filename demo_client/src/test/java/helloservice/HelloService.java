package helloservice;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.acme.Person;

public class HelloService {

	WebTarget baseUrl;

	public HelloService() {
		Client client = ClientBuilder.newClient();
		baseUrl = client.target("http://localhost:8080/");
	}
	
	public String hello() {
		return baseUrl.path("hello").request().get(String.class);
	}

	public Person getPersonJson() {
		return getPerson(MediaType.APPLICATION_JSON);
	}
	
	public Person getPersonXml() {
		return getPerson(MediaType.APPLICATION_XML);
	}
	
	public Person getPerson(String mediaType) {
		return baseUrl.path("hello")
				.path("person")
				.request()
				.accept(mediaType)
				.get(Person.class);
	}
}
