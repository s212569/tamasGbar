package helloservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.acme.Person;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/* Hint:
 * The step classes do not do the HTTP requests themselves.
 * Instead, the tests use the class HelloService, which encapsulates the 
 * HTTP requests. This abstractions help to write easier and more understandable 
 * test classes.
 */
public class HelloServiceSteps {

	String result;
	Person person;
	HelloService service = new HelloService();

	@When("I call the hello service")
	public void iCallTheHelloService() {
		result = service.hello();
	}

	@Then("I get the answer {string}")
	public void iGetTheAnswer(String string) {
		assertEquals(string, result);
	}

	@When("I call the hello service to get person via Xml")
	public void iCallTheHelloServiceToGetPersonViaXml() {
		person = service.getPersonJson();
	}

	@Then("I get a person with name {string} and address {string}")
	public void iGetAPersonWithNameAndAddress(String name, String address) {
		assertEquals(name, person.getName());
		assertEquals(address, person.getAddress());
	}

	@When("I call the hello service to get person via Json")
	public void iCallTheHelloServiceToGetPersonViaJson() {
		person = service.getPersonXml();
	}

}
