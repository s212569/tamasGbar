package org.acme;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello RESTEasy";
    }
    
    @GET
    @Path("person")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getPersonJson() {
    	return getPerson();
    }
    @GET
    @Path("person")
    @Produces(MediaType.APPLICATION_XML)
    public Person getPersonXml() {
    	return getPerson();
    }
    
    public Person getPerson() {
    	return new Person("Hopper","Garden City");
    }
}